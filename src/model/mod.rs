use serde::{Deserialize, Serialize};

use self::alert::AlertGroup;

pub mod alert;
pub mod config;

#[derive(Default, Serialize, Deserialize, Clone)]
pub struct AppState {
    pub alerts: Vec<AlertGroup>,
}
