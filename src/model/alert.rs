use serde::{Deserialize, Serialize};

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Alert {
    pub status: String, // TODO use enum
    // labels:
    // annotations:
    pub startsAt: String,
    pub endsAt: String,
    pub generatorURL: String,
    pub fingerprint: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlertGroup {
    pub version: String,
    pub groupKey: String,
    pub truncatedAlerts: u32,
    pub status: String, // TODO use enum
    pub receiver: String,
    // groupLabels:
    // commonLabels:
    // commonAnnotations;:
    pub alerts: Vec<Alert>,
    pub externalURL: String,
}
