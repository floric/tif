use crate::views::errors::*;
use crate::views::home::*;
use crate::views::playground::*;
use leptos::*;
use leptos_meta::*;
use leptos_router::*;

#[component]
pub fn App(cx: Scope) -> impl IntoView {
    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context(cx);

    view! {
        cx,
        <Stylesheet id="leptos" href="/pkg/tif.css"/>
        <Title text="BahnXyz Services | Status"/>
        <Router>
            <main>
                <Routes>
                    <Route path="/" view=|cx| view! { cx, <HomePage/> } />
                    <Route path="/playground" view=|cx| view! { cx, <Playground/> } />
                    <Route path="/404" view=|cx| view! { cx, <Error404Page/> }/>
                </Routes>
            </main>
        </Router>
    }
}
