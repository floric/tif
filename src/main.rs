#[cfg(feature = "ssr")]
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    use actix_files::Files;
    use actix_web::{middleware::*, *};
    use leptos::*;
    use leptos_actix::{generate_route_list, LeptosRoutes};
    use log::info;
    use std::sync::Mutex;
    use tif::{
        app::*,
        model::{alert::AlertGroup, AppState},
    };

    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let conf = get_configuration(None).await.unwrap();
    let addr = conf.leptos_options.site_address;

    let routes = generate_route_list(|cx| view! { cx, <App/> });

    #[post("/api/hooks/alertmanager")]
    async fn post_alert_group(
        alert_group: web::Json<AlertGroup>,
        data: web::Data<Mutex<AppState>>,
    ) -> impl Responder {
        info!("Received {} new alerts", alert_group.0.alerts.len());

        data.lock().unwrap().alerts.push(alert_group.clone());

        HttpResponse::NoContent()
    }

    #[get("/api/alertgroups")]
    async fn get_alert_groups(data: web::Data<Mutex<AppState>>) -> impl Responder {
        let alert_groups = data.lock().unwrap().alerts.clone();

        web::Json(alert_groups)
    }

    HttpServer::new(move || {
        let leptos_options = &conf.leptos_options;
        let site_root = &leptos_options.site_root;
        let app_state = web::Data::new(Mutex::new(AppState::default()));
        info!("initialized worker");

        App::new()
            .wrap(Logger::default())
            .app_data(app_state)
            .service(post_alert_group)
            .service(get_alert_groups)
            .route("/api/{tail:.*}", leptos_actix::handle_server_fns())
            .leptos_routes(
                leptos_options.to_owned(),
                routes.to_owned(),
                |cx| view! { cx, <App/> },
            )
            .service(Files::new("/", site_root))
            .wrap(middleware::Compress::default())
    })
    .bind(&addr)?
    .workers(1)
    .run()
    .await
}

#[cfg(not(feature = "ssr"))]
pub fn main() {
    // no client-side main function
    // unless we want this to work with e.g., Trunk for pure client-side testing
    // see lib.rs for hydration function instead
}
