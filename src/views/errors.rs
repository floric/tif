use leptos::*;

#[component]
pub fn Error404Page(cx: Scope) -> impl IntoView {
    view! { cx,
        <h1>"Seite nicht gefunden."</h1>
        <p>"Der Link zu dieser Seite scheint leider fehlerhaft zu sein."</p>
    }
}
