use leptos::*;

use crate::model::alert::AlertGroup;

#[component]
pub fn HomePage(cx: Scope) -> impl IntoView {
    let alert_groups = create_local_resource(
        cx,
        || 1,
        |_| async {
            let alert_groups = reqwasm::http::Request::get("http://localhost:3000/api/alertgroups")
                .send()
                .await
                .map_err(|err| err.to_string())
                .unwrap()
                .json::<Vec<AlertGroup>>()
                .await
                .map_err(|err| err.to_string());

            alert_groups
        },
    );

    view! { cx,
        <div class="page">
            <div class="content">
                <div class="headline">
                    <img class="logo" src="images/bahnxyz_logo.png" alt="Logo" />
                    <h1 class="title">"BahnXyz Services"</h1>
                </div>

                <h2 class="subtitle offsetLeft">"Status"</h2>
                <div class="card offsetLeft">
                    {move || {
                        if alert_groups().is_some() {
                            if alert_groups().unwrap().is_ok() {
                                if alert_groups().unwrap().unwrap().len() > 0 {
                                    view! { cx,
                                        <div class="status-line">
                                            <svg class="icon error" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3.75m9-.75a9 9 0 11-18 0 9 9 0 0118 0zm-9 3.75h.008v.008H12v-.008z" />
                                            </svg>
                                            <div>
                                                <p>"Es wurden automatisiert " {alert_groups().unwrap().unwrap().len()} " Störungen gemeldet. Die Entstörung wurde eingeleitet."</p>
                                                <h3 class="details">"Details"</h3>
                                                <p>"Erste Störung gemeldet am 01.02.23 um 08:57 Uhr"</p>
                                            </div>
                                        </div>
                                    }
                                } else {
                                    view! { cx,
                                        <div class="status-line">
                                            <svg class="icon success" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
                                                <path fill-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clip-rule="evenodd" />
                                            </svg>
                                            <p>"Es wurden derzeit Problem gemeldet."</p>
                                        </div>
                                    }
                                }.into_view(cx)
                            } else {
                                view! { cx,
                                    <>"Fehler: " {alert_groups().unwrap().unwrap_err()}</>
                                }.into_view(cx)
                            }
                        } else {
                            (view! { cx, <>"Loading..."</> }).into_view(cx)
                        }
                    }}
                </div>

                <h2 class="subtitle offsetLeft">"Partnersysteme"</h2>
                <div class="card offsetLeft">
                    <div class="partners">
                        <div class="partner-item">
                            <svg class="icon success" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
                                <path fill-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clip-rule="evenodd" />
                            </svg>
                            "System A"
                        </div>
                        <div class="partner-item">
                            <svg class="icon success" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
                                <path fill-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clip-rule="evenodd" />
                            </svg>
                            "System B"
                        </div>
                        <div class="partner-item">
                            <svg class="icon success" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
                                <path fill-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clip-rule="evenodd" />
                            </svg>
                            "System C"
                        </div>
                    </div>
                </div>

                <h2 class="subtitle offsetLeft">"Vergangene Ereignisse"</h2>
                <ul class="timeline">
                    <li class="event"><div class="circle" />
                        <div class="card">
                            <div class="incident">
                                <svg class="icon error" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3.75m9-.75a9 9 0 11-18 0 9 9 0 0118 0zm-9 3.75h.008v.008H12v-.008z" />
                                </svg>
                                <div>
                                    <div class="incidentDate">"29.01.23"</div>
                                    <div class="incidentDescription">"Die Partnerschnittstelle A lieferte zwischen 10:05 Uhr und 16:32 Uhr fehlerhafte Nutzerdaten. Aus diesem Grund kam es bei rund zwanzig Prozent der Anfragen zu Fehlern."<br />"Ein Fix der Partnerschnittstelle hat den Fehler beseitigt."</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="event"><div class="circle" />
                        <div class="card">
                            <div class="incident">
                                <svg class="icon warning" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3.75m-9.303 3.376c-.866 1.5.217 3.374 1.948 3.374h14.71c1.73 0 2.813-1.874 1.948-3.374L13.949 3.378c-.866-1.5-3.032-1.5-3.898 0L2.697 16.126zM12 15.75h.007v.008H12v-.008z" />
                                </svg>
                                <div>
                                    <div class="incidentDate">"23.12. - 28.12.22"</div>
                                    <div class="incidentDescription">"Vereinzelte Fehler im Login-Prozess. Wurden aber durch Partnersystem behoben."</div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    }
}
