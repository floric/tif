use crate::views::components::notification::*;
use leptos::*;

async fn post_test_alert(status: &str) -> Result<(), String> {
    let _res = reqwasm::http::Request::post("http://localhost:3000/api/hooks/alertmanager")
        .body(format!(
            r#"{{
            "version": "4",
            "groupKey": "key",
            "truncatedAlerts": 1,
            "status": "{status}",
            "receiver": "some",
            "externalURL": "https://some.de",
            "alerts": [
              {{
                "status": "firing",
                "startsAt": "2020-12-09 16:09:53+00:00",
                "endsAt": "2020-12-09 18:09:53+00:00",
                "generatorURL": "https://some.de",
                "fingerprint": "abcde"
              }}
            ]
          }}"#,
        ))
        .header("content-type", "application/json")
        .send()
        .await
        .map_err(|err| err.to_string())?;

    Ok(())
}

#[component]
pub fn Playground(cx: Scope) -> impl IntoView {
    let (send_alert_message, set_send_alert_message) =
        create_signal::<Option<Notification>>(cx, None);

    let fire_action = create_action(cx, |_| post_test_alert("firing"));
    let resolve_action = create_action(cx, |_| post_test_alert("resolving"));

    let on_send_firing_alert = move |_| {
        fire_action.dispatch(());
    };

    create_effect(cx, move |_| {
        let res = fire_action.value();
        if res().is_some() {
            match res().unwrap() {
                Ok(_) => {
                    set_send_alert_message(Some(Notification::Success(
                        "Alert sent successfully.".to_string(),
                    )));
                }
                Err(_) => {
                    set_send_alert_message(Some(Notification::Error(
                        "Alert sending failed.".to_string(),
                    )));
                }
            }
        }
    });

    let on_send_resolving_alert = move |_| {
        resolve_action.dispatch(());
    };

    view! { cx,
        <div class="page">
            <div class="content">
                <div class="headline">
                    <h1 class="title">"Playground"</h1>
                </div>

                <h2 class="subtitle">"Test Alerts"</h2>
                <div class="card">
                    {move || {
                        if send_alert_message().is_some() {
                            view! { cx, <Notification notification=send_alert_message().unwrap() />}.into_view(cx)
                        } else {
                            ().into_view(cx)
                        }
                    }}
                    <div class="button-group">
                        <button on:click=on_send_firing_alert disabled=fire_action.pending()>"Fire Alert"</button>
                        <button on:click=on_send_resolving_alert disabled=resolve_action.pending()>"Resolve Alert"</button>
                    </div>
                </div>
            </div>
        </div>
    }
}
