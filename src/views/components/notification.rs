use leptos::*;

#[derive(Clone)]
pub enum Notification {
    Success(String),
    Warning(String),
    Error(String),
}

#[component]
pub fn Notification(cx: Scope, notification: Notification) -> impl IntoView {
    let (dismissed, set_dismissed) = create_signal(cx, false);
    let on_dismiss = move |_| {
        set_dismissed(true);
    };

    view! { cx,
        <div> { move || if !dismissed.get() {
                    view! { cx, <div>{
                        match notification.clone() {
                            Notification::Success(message) => view! {cx,
                                <div class="notification success-bg">
                                    <div class="notification-message">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="icon">
                                            <path fillRule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clipRule="evenodd" />
                                        </svg>
                                        {message}
                                    </div>
                                    <button on:click=on_dismiss>"Dismiss"</button>
                                </div>},
                            Notification::Warning(message) => view! {cx,
                                <div class="notification warning-bg">
                                    <div class="notification-message">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" fill="transparent" class="icon">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v3.75m-9.303 3.376c-.866 1.5.217 3.374 1.948 3.374h14.71c1.73 0 2.813-1.874 1.948-3.374L13.949 3.378c-.866-1.5-3.032-1.5-3.898 0L2.697 16.126zM12 15.75h.007v.008H12v-.008z" />
                                        </svg>
                                        {message}
                                    </div>
                                    <button on:click=on_dismiss>"Dismiss"</button>
                                </div>},
                            Notification::Error(message) => view! {cx,
                                <div class="notification error-bg">
                                    <div class="notification-message">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke="currentColor" fill="transparent" class="icon">
                                            <path fillRule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zM12 8.25a.75.75 0 01.75.75v3.75a.75.75 0 01-1.5 0V9a.75.75 0 01.75-.75zm0 8.25a.75.75 0 100-1.5.75.75 0 000 1.5z" clipRule="evenodd" />
                                        </svg>
                                        {message}
                                    </div>
                                </div>},
                            }
                        }
                    </div>}.into_view(cx)
                } else {
                    ().into_view(cx)
                }
            }
        </div>
    }
}
